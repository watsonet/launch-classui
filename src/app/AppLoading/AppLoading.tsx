import * as React from 'react';
import { Spinner, Title, EmptyState, EmptyStateIcon } from '@patternfly/react-core';

const AppLoading: React.FunctionComponent = () => {
  // TODO make this a nice page or something.

  return (
    <EmptyState>
      <EmptyStateIcon variant="container" component={Spinner} />
      <Title size="lg" headingLevel="h4">
        Loading Merge Launch
      </Title>
    </EmptyState>
  );
};

export { AppLoading };

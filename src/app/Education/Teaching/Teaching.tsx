import { GeneralSettingsContext } from "@app/Settings/General/GeneralSettings";
import { AuthContext } from "@app/lib/AuthProvider";
import { User, GetUserResponse, Organization, GetOrganizationResponse, Member_Role } from "@mergetb/api/portal/v1/workspace_types";
import { Breadcrumb, BreadcrumbItem, PageSection, Title } from "@patternfly/react-core";
import { Thead, Tr, Th, Tbody, Td, Table, TableComposable } from '@patternfly/react-table';
import React from "react";
import { Link } from "react-router-dom";
import { useFetch } from "use-http";

interface ClassOrgProps {
  oid?: string;
  uid?: string;
  category?: string;
}

const ClassOrg: React.FunctionComponent<ClassOrgProps> = ({ oid = "", uid = "" }) => {
  const conf = React.useContext(GeneralSettingsContext);
  const options = { credentials: 'include', cache: 'no-cache' };
  const { get, response } = useFetch(`${conf.api}/organization/${oid}`, options);
  const [org, setOrg] = React.useState<Organization>();

  const getOrg = React.useCallback(async () => {
    const data = await get();
    if (response.ok && Object.hasOwnProperty.call(data, 'organization')) {
      setOrg(GetOrganizationResponse.fromJSON(data).organization);
    }
  }, [response, get]);

  React.useEffect(() => { getOrg() }, [getOrg]);

  const filterOrgs = (org: (Organization | undefined)) => {
    if (org == undefined || org.category.toLowerCase() != 'class') {
      return null;
    } else if (org.members[uid] == undefined || org.members[uid].role == Member_Role.Member || org.members[uid].role == Member_Role.UNRECOGNIZED) {
      return null;
    }

    return (
      <Link to={`/class/${oid}`}>
        {org.name}
      </Link>
    );

  }

  return filterOrgs(org);
}

const Teaching: React.FunctionComponent = () => {
  const { identity } = React.useContext(AuthContext);
  const username = identity?.traits.username;

  const conf = React.useContext(GeneralSettingsContext);
  const options = { credentials: 'include', cache: 'no-cache' };
  const { get, response } = useFetch(`${conf.api}/user/${username}`, options);
  const [user, setUser] = React.useState<User>();

  const getUser = React.useCallback(async () => {
    const data = await get();
    if (response.ok && Object.hasOwnProperty.call(data, 'user')) {
      setUser(GetUserResponse.fromJSON(data).user);
    }
  }, [response, get]);

  React.useEffect(() => { getUser() }, [getUser]);

  return (
    <React.Fragment>
      <PageSection>
        <Breadcrumb>
          <BreadcrumbItem isActive>Manage Classes</BreadcrumbItem>
        </Breadcrumb>
        <Title headingLevel="h1">
          Teaching
        </Title>
        <TableComposable aria-label='Class Table' variant={'compact'} borders={false}>
          <Thead>
            <Tr>
              <Th>
                Select Class to Manage
              </Th>
            </Tr>
          </Thead>
          <Tbody>
            {Object.keys(
              user !== undefined ? user.organizations : {}
            ).map((o, i) =>
              <Tr key={i}>
                <Td key={i}>
                  <ClassOrg oid={o} uid={user?.username} />
                </Td>
              </Tr>
            )}
          </Tbody>
        </TableComposable>
      </PageSection>
    </React.Fragment>
  );
};

export { Teaching };
import * as React from 'react';
import { FileUpload, PageSection, Title } from "@patternfly/react-core"
import { AuthContext } from '@app/lib/AuthProvider';
import { GeneralSettingsContext } from '@app/Settings/General/GeneralSettings';
import { useFetch } from 'use-http';
import { AccessMode, GetOrganizationResponse, GetUserResponse, Member_Role, Organization, User } from '@mergetb/api/portal/v1/workspace_types';
import { Link } from 'react-router-dom';
import { Thead, Tr, Th, Tbody, Td, Table } from '@patternfly/react-table';
import { Material, Assignment, Submission, FileData } from '@buf/sphere_edu.bufbuild_es/edu/v1/edu_types_pb';
import { GetMaterialsResponse, GetAssignmentsResponse, UploadFileRequest, UploadSubmissionRequest } from '@buf/sphere_edu.bufbuild_es/edu/v1/edu_pb';
import { SubmissionFileUpload } from '@app/lib/SubmissionFileUpload';
import { EduService } from "@buf/sphere_edu.connectrpc_es/edu/v1/edu_connect";
import { createPromiseClient } from "@connectrpc/connect"
import { createConnectTransport } from '@connectrpc/connect-web'

interface ClassOrgProps {
	oid?: string;
	uid?: string;
	category?: string;
	isProfOrTA?: boolean;
	isAdmin?: boolean;
}


const getUserAssignments: React.FunctionComponent<ClassOrgProps> = ({ uid = "", oid = "", category = "", isProfOrTA = false }) => {
	// Do API call here when implemented
	const conf = React.useContext(GeneralSettingsContext);
	const transport = createConnectTransport({
		baseUrl: `${conf.eduApi}`,
	})
	const client = createPromiseClient(EduService, transport);
	const [assignments, setAssignments] = React.useState<Assignment[]>([]);

	React.useMemo(async () => {
		if (oid == "") return;

		await client.getAssignments({
			classId: oid,
		}).then(response => {
			setAssignments(response.assignments)
		}).catch(error => error)
	}, [oid]);

	// Put after the React hooks to prevent React from bitching
	if (oid == "" || category.toLowerCase() != 'class') return (<React.Fragment />);

	const cols = {
		title: "Title",
		dueDate: "Due",
		releaseDate: "Released",
		submission: "Submitted",
		submit: "Submit",
	};

	return (
		<React.Fragment>
			<Title headingLevel='h2'>
				{"Assignments for "}
				<Link to={`/organization/${oid}`}>
					{oid}
				</Link>
			</Title>
			<Table aria-label='Class Assignments' variant={'compact'} borders={false} key={`assignments-table-${oid}`}>
				<Thead key={`assignments-table-head-${oid}`}>
					<Tr>
						<Th key={1}>{cols.title}</Th>
						<Th key={2}>{cols.dueDate}</Th>
						<Th key={3}>{cols.releaseDate}</Th>
						<Th key={4}>{cols.submission}</Th>
						<Th key={5}>{cols.submit}</Th>
					</Tr>
				</Thead>
				{assignments.map((a, i) => {
					if (!a.isVisibleToAll && !isProfOrTA) {
						return;
					}
					return (<Tbody key={`assignments-table-body-${oid}-${i}`}>
						<Tr>
							<Td key={6 * i + 1} dataLabel={cols.title}>
								<Link to={{ pathname: `${a.material?.url}` }} target='_blank'>
									{a.displayName}
								</Link>
							</Td>
							<Td key={6 * i + 3} dataLabel={cols.dueDate}>
								{a.dueDate?.toDate().toLocaleString()}
							</Td>
							<Td key={6 * i + 4} dataLabel={cols.releaseDate}>
								{a.releaseDate?.toDate().toLocaleString()}
							</Td>
							<Td key={6 * i + 5} dataLabel={cols.submission}>
								{/* {a.submission.submitted ? `Submitted at ${a.submission.date?.toLocaleString()}` : ""} */}
								{a.submissions[uid] == undefined ? "No submission" : a.submissions[uid]}
							</Td>
							<Td key={6 * i + 6} dataLabel={cols.submit}>
								<SubmissionFileUpload classId={oid} userId={uid} assignmentId={a.assignmentId} />

							</Td>
						</Tr>
					</Tbody>)
				})}
			</Table>
		</React.Fragment>
	)
}

const getClassMaterials: React.FunctionComponent<ClassOrgProps> = ({ oid = "", category = "", isProfOrTA = false }) => {
	// Do API call here when implemented

	const conf = React.useContext(GeneralSettingsContext);
	const transport = createConnectTransport({
		baseUrl: `${conf.eduApi}`,
	})
	const client = createPromiseClient(EduService, transport);
	const [materials, setMaterials] = React.useState<Material[]>([]);

	React.useMemo(async () => {
		if (oid == "") return;

		await client.getMaterials({
			classId: oid,
		}).then(response => {
			setMaterials(response.materials);
		}).catch(error => error);
	}, [oid]);

	// Put after the React hooks to prevent React from bitching
	if (oid == "" || category.toLowerCase() != 'class') return (<React.Fragment />);

	const cols = {
		title: "Title",
	};

	return (
		<React.Fragment>
			<Title headingLevel='h2'>
				{"Materials for "}
				<Link to={`/organization/${oid}`}>
					{oid}
				</Link>
			</Title>
			<Table aria-label={`Class Materials ${oid}`} variant={'compact'} borders={false} key={`materials-table-${oid}`}>
				<Thead key={`materials-table-head-${oid}`}>
					<Tr>
						<Th>{cols.title}</Th>
					</Tr>
				</Thead>
				{materials.map((mat, i) => {
					if (!mat.isVisibleToAll && !isProfOrTA) {
						return;
					}
					return (<Tbody key={`materials-table-body-${oid}-${i}`}>
						<Tr>
							<Td dataLabel={cols.title}>
								<Link to={{ pathname: `${mat.url}` }} target='_blank'>
									{mat.displayName}
								</Link>
							</Td>
						</Tr>
					</Tbody>)
				})}
			</Table>
		</React.Fragment>
	)
}

const ClassOrg: React.FunctionComponent<ClassOrgProps> = ({ oid = "", uid = "", isAdmin = false }) => {
	const conf = React.useContext(GeneralSettingsContext);
	const [org, setOrg] = React.useState<Organization>();

	React.useMemo(async () => {
		const data = await fetch(`${conf.api}/organization/${oid}`, {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json'
			},
			credentials: 'include',
			cache: 'no-cache',
		}).then(response => response.json())
		// console.log(data);

		if (Object.hasOwnProperty.call(data, 'organization')) {
			setOrg(GetOrganizationResponse.fromJSON(data).organization);
		}
	}, [oid]);

	const isProfOrTA = (uid: string, org: Organization | undefined) => {
		if (!org) {
			return false;
		}
		if (isAdmin) {
			return true;
		} else if (org.members[uid].role == Member_Role.Creator || org.members[uid].role == Member_Role.Maintainer) {
			return true;
		}
		return false;
	}

	return (
		<React.Fragment>
			{getClassMaterials({ oid: org?.name, category: org?.category, uid: uid, isProfOrTA: isProfOrTA(uid, org) })}
			{getUserAssignments({ oid: org?.name, category: org?.category, uid: uid, isProfOrTA: isProfOrTA(uid, org) })}
		</React.Fragment>);
}

const MyClasses: React.FunctionComponent = () => {
	const { identity } = React.useContext(AuthContext);
	const username = identity?.traits.username;

	const conf = React.useContext(GeneralSettingsContext);
	const [user, setUser] = React.useState<User>();

	React.useMemo(async () => {
		if (username == undefined) {
			return;
		}
		const data = await fetch(`${conf.api}/user/${username}`, {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json'
			},
			credentials: 'include',
			cache: 'no-cache',
		}).then(response => response.json())

		if (Object.hasOwnProperty.call(data, 'user')) {
			setUser(GetUserResponse.fromJSON(data).user);
		}
	}, [username]);

	return (
		<React.Fragment>
			<PageSection>
				<Title headingLevel="h1">
					My Classes
				</Title>
				{Object.keys(
					user ? user.organizations : {}
				).map((o, i) =>
					<ClassOrg oid={o} uid={user?.username} key={`class-${o}`} />
				)}
			</PageSection>
		</React.Fragment>
	)
};

export { MyClasses };
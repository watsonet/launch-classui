import { GeneralSettingsContext } from '@app/Settings/General/GeneralSettings';
import { GetOrganizationResponse } from '@mergetb/api/portal/v1/workspace_types';
import * as React from 'react';
import useFetch from 'use-http';

// type UserUndefined = User | undefined;

export function isMemberOfClass(oids: string[] = []): boolean {

	const [result, setResult] = React.useState<boolean>(false);

	React.useEffect(() => {
		oids.map((oid) => {
			const conf = React.useContext(GeneralSettingsContext);
			const options = { credentials: 'include', cachePolicy: 'no-cache' };
			const url = conf.api + '/organization/' + oid
			const { loading, error, data } = useFetch(url, options);
			const org = React.useMemo(() => {
				if (data && Object.hasOwnProperty.call(data, 'organization')) {
					return GetOrganizationResponse.fromJSON(data).organization;
				}
				return GetOrganizationResponse.fromJSON({}).organization;
			}, [data]);

			if (org) {
				console.log(org.category);
				if (org.category == 'class') {
					setResult(true);
				}
			}
		})
	}, [oids, conf, options]);

	return result;
} 
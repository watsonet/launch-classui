import { GeneralSettingsContext } from "@app/Settings/General/GeneralSettings";
import { AuthContext } from "@app/lib/AuthProvider";
import { GetClassResponse } from "@buf/sphere_edu.bufbuild_es/edu/v1/edu_pb";
import { Class, UserType } from "@buf/sphere_edu.bufbuild_es/edu/v1/edu_types_pb";
import { ActionGroup, Button, Form, FormGroup, PageSection, Radio, TextArea, Title, Breadcrumb, BreadcrumbItem, Alert, Bullseye, Spinner, } from "@patternfly/react-core";
import * as React from "react";
import { useParams } from "react-router-dom";

type AddUserProps = {
  classId: string;
}

const AddUsers: React.FunctionComponent<AddUserProps> = () => {
  const { classId } = useParams<AddUserProps>();
  const conf = React.useContext(GeneralSettingsContext);
  const { identity } = React.useContext(AuthContext);
  const username = identity?.traits.username;

  const [emails, setEmails] = React.useState('');
  const [response, setResponse] = React.useState<Response>();
  const [classroom, setClassroom] = React.useState<Class>();

  React.useMemo(async () => {
    const data = await fetch(`${conf.eduApi}/edu.v1.EduService/GetClass`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        classId: classId,
      }),
      credentials: 'include',
      cache: 'no-cache',
    }).then(response => {
      setResponse(response);
      return response.json();
    })
    // console.log(data);

    if (Object.hasOwnProperty.call(data, 'class')) {
      setClassroom(GetClassResponse.fromJson(data).class);
    }
  }, [classId]);

  if (classroom == undefined) {
    if (response && response.ok) {
      return <React.Fragment>
        <PageSection>
          <Alert variant="danger" title="Not Found Error">
            <pre>This class does not exist</pre>
          </Alert>
        </PageSection>
      </React.Fragment>
    } else {
      return <React.Fragment>
        <Bullseye>
          <Spinner size='xl' />
        </Bullseye>
      </React.Fragment>
    }
  } else if (classroom.users[username] == undefined || classroom.users[username] == UserType.STUDENT || classroom.users[username] == UserType.UNSPECIFIED) {
    return <React.Fragment>
      <PageSection>
        <Alert variant="danger" title="Permission Error">
          <pre>You do not have permission to access this content</pre>
        </Alert>
      </PageSection>
    </React.Fragment>
  }

  const handleEmailsChange = (value: string, _event) => {
    setEmails(value);
  };


  const handleSubmit = (_event) => {
    const emailArray: string[] = emails.split('\n');
    // console.log(emailArray.toString());

    const test = new RegExp(/^[\w-+\.]+@([\w-]+\.)+[\w-]{2,4}$/);
    emailArray.map(e => {
      e = e.trim();

      if (e == "") {
        return;
      }

      console.log(`${e}: ${test.test(e)}`);

      // TODO: Implement the process for emailing a user and all that


      // const options = { credentials: 'include' };
    })
  }

  return <React.Fragment>
    <PageSection>
      <Breadcrumb>
        <BreadcrumbItem to="/edu/teaching">Manage Classes</BreadcrumbItem>
        <BreadcrumbItem to={`/class/${classId}`}>Class {classId}</BreadcrumbItem>
        <BreadcrumbItem isActive>Add Users to {classId}</BreadcrumbItem>
      </Breadcrumb>
      <Title headingLevel="h1">
        Add users to {classId}
      </Title>
      <Form>
        <FormGroup
          label="Emails"
          isRequired
          fieldId="simple-form-email-01"
          helperText="One email per line"
        >
          <TextArea
            isRequired
            id="simple-form-email-01"
            name="simple-form-email-01"
            value={emails}
            onChange={handleEmailsChange}
          />
        </FormGroup>
        <FormGroup isRequired role="radiogroup" isInline fieldId="basic-form-radio-group" label="Students or TAs?">
          <Radio name="basic-inline-radio" label="Students" id="basic-inline-radio-01" />
          <Radio name="basic-inline-radio" label="TAs" id="basic-inline-radio-02" />
        </FormGroup>
        <ActionGroup>
          <Button variant="primary" onClick={handleSubmit}>Submit</Button>
        </ActionGroup>
      </Form>
    </PageSection>
  </React.Fragment>
};

export { AddUsers };
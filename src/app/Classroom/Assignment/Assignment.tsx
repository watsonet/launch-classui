import { ActionGroup, Button, Form, FormGroup, PageSection, Radio, Title, Breadcrumb, BreadcrumbItem, TextInput, Alert, Bullseye, Spinner, Dropdown, DropdownItem, DropdownSeparator, DropdownToggle, FormSelect, FormSelectOption, DatePicker, Modal, ModalVariant, FormAlert, TimePicker, } from "@patternfly/react-core";
import * as React from "react";
import { useParams } from "react-router-dom";
import { Class, UserType, Material, Assignment } from "@buf/sphere_edu.bufbuild_es/edu/v1/edu_types_pb";
import { GetClassResponse, GetMaterialsResponse, GetAssignmentResponse } from "@buf/sphere_edu.bufbuild_es/edu/v1/edu_pb";
import { Timestamp } from '@bufbuild/protobuf';
import { AuthContext } from "@app/lib/AuthProvider";
import { GeneralSettingsContext } from "@app/Settings/General/GeneralSettings";
import { ExclamationCircleIcon } from "@patternfly/react-icons";
import { EduService } from "@buf/sphere_edu.connectrpc_es/edu/v1/edu_connect";
import { createPromiseClient } from "@connectrpc/connect"
import { createConnectTransport } from '@connectrpc/connect-web'

type AssignmentProps = {
  classId: string;
  assignmentId: string;
}

type ValidatedProps = "default" | "success" | "warning" | "error";

const AssignmentPage: React.FunctionComponent<AssignmentProps> = () => {
  const { classId, assignmentId } = useParams<AssignmentProps>();
  const { identity } = React.useContext(AuthContext);

  const conf = React.useContext(GeneralSettingsContext);
  const username = identity?.traits.username;
  const transport = createConnectTransport({
    baseUrl: `${conf.eduApi}`,
  })
  const client = createPromiseClient(EduService, transport);

  const [response, setResponse] = React.useState<boolean>();

  const [material, setMaterial] = React.useState<Material>();
  const [assignment, setAssignment] = React.useState<Assignment>();
  const [dueDate, setDueDate] = React.useState<string>("");
  const [dueTime, setDueTime] = React.useState<number>(0);
  const [selected, setSelected] = React.useState("default");
  const [classroom, setClassroom] = React.useState<Class>();
  const [materials, setMaterials] = React.useState<Map<string, Material>>(new Map());
  const [visibility, setVisibility] = React.useState<string>("");
  const [releaseDate, setReleaseDate] = React.useState<string>("");
  const [releaseTime, setReleaseTime] = React.useState<number>(0);
  const [dropdownItems, setDropdownItems] = React.useState<JSX.Element[]>([]);
  const [assignmentName, setAssignmentName] = React.useState<string>("");

  const [isModalOpen, setIsModalOpen] = React.useState<boolean>(false);
  const [formValidated, setFormValidated] = React.useState<ValidatedProps>('default');
  const [nameValidated, setNameValidated] = React.useState<ValidatedProps>('default');
  const [materialValidated, setMaterialValidated] = React.useState<ValidatedProps>('default');
  const [dueDateValidated, setDueDateValidated] = React.useState<ValidatedProps>('default');
  const [releaseDateValidated, setReleaseDateValidated] = React.useState<ValidatedProps>('default');
  const [visibilityValidated, setVisibilityValidated] = React.useState<ValidatedProps>('default');
  const [dueDateHelperText, setDueDateHelperText] = React.useState<string>('');
  const [releaseDateHelperText, setReleaseDateHelperText] = React.useState<string>('');

  const timestampToDateString = (date: Date | undefined) => {
    let dateString = "";
    if (date != undefined) {
      dateString = `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`
    }
    return dateString;
  }

  React.useEffect(() => {
    const fetchData = async () => {
      await client.getClass({
        classId: classId,
      }).then(async response => {
        setResponse(true);
        setClassroom(response.class);

        await client.getMaterials({
          classId: classId,
        }).then(response => {
          response.materials.map(m => {
            materials.set(m.materialId, m);
            setMaterials(materials);
          });
        }).catch(error => error)

        return response;
      }).catch(error => error)

      dropdownItems.push(<FormSelectOption key={"default-option"} value={'default'} label={"Select Material to Assign"} />)
      setDropdownItems(dropdownItems);

      materials.forEach((m, k) => {
        console.log(k)
        dropdownItems.push(<FormSelectOption key={k} value={k} label={m.displayName} />)
        setDropdownItems(dropdownItems);
      })

      await client.getAssignment({
        assignmentId: assignmentId,
        classId: classId,
      }).then(response => {
        const a = response.assignment;
        if (a) {
          setAssignment(a);
          setAssignmentName(a.displayName);
          setSelected(a.material?.materialId || "");
          setVisibility(a.isVisibleToAll ? "all" : "prof");
          setDueDate(timestampToDateString(a.dueDate?.toDate()))
          setReleaseDate(timestampToDateString(a.releaseDate?.toDate()))
        }
      }).catch(error => error)
    }

    fetchData();
  }, [classId, assignmentId]);


  if (classroom == undefined) {
    if (response) {
      return <React.Fragment>
        <PageSection>
          <Alert variant="danger" title="Not Found Error">
            <pre>This class does not exist</pre>
          </Alert>
        </PageSection>
      </React.Fragment>
    } else {
      return <React.Fragment>
        <Bullseye>
          <Spinner size='xl' />
        </Bullseye>
      </React.Fragment>
    }
  } else if (classroom.users[username] == undefined || classroom.users[username] == UserType.STUDENT || classroom.users[username] == UserType.UNSPECIFIED) {
    return <React.Fragment>
      <PageSection>
        <Alert variant="danger" title="Permission Error">
          <pre>You do not have permission to access this content</pre>
        </Alert>
      </PageSection>
    </React.Fragment>
  }

  const handleAssignmentName = (value: string) => {
    setAssignmentName(value);
    value = value.trim();
    if (value == '') {
      setNameValidated('error')
    } else {
      setNameValidated('success');
    }
  };

  const onSelect = (value: string) => {
    setSelected(value);
    if (value == 'default') {
      setMaterialValidated('error')
    } else {
      setMaterialValidated('success')
    }
  };

  const onDueDateChange = (value: string, date: Date | undefined) => {
    setDueDate(value);
    if (date == undefined) {
      setDueDateValidated('error');
      setDueDateHelperText('');
      return;
    }
    const dueDateTimeValue = date.valueOf() + 1000 * dueTime
    if (dueDateTimeValue < Date.now()) {
      setDueDateValidated('warning');
      setDueDateHelperText('Due date and time cannot be before current time');
    } else {
      setDueDateValidated('success');
      setDueDateHelperText('');
    }
  }

  const onDueTimeChange = (time: string, hour?: number | undefined, minute?: number | undefined, seconds?: number | undefined, isValid?: boolean | undefined) => {
    if (isValid) {
      if (hour != undefined && minute != undefined) {
        const dueTime = hour * 3600 + minute * 60;
        setDueTime(dueTime);
        const dueDateValue = new Date(Date.parse(dueDate))
        if (!Number.isNaN(dueDateValue)) {
          if (dueDateValue.valueOf() + 60000 * dueDateValue.getTimezoneOffset() + 1000 * dueTime < Date.now()) {
            setDueDateValidated('warning');
            setDueDateHelperText('Due date and time cannot be before current time');
            return;
          }
        }
        setDueDateValidated('success');
        setDueDateHelperText('');
      } else {
        setDueDateValidated('error');
        setDueDateHelperText('');
      }
    } else {
      setDueDateValidated('error');
      setDueDateHelperText('');
    }
  }

  const onReleaseDateChange = (value: string, date: Date | undefined) => {
    setReleaseDate(value);
    if (date == undefined) {
      setReleaseDateValidated('error');
      setReleaseDateHelperText('');
      return;
    }
    const dueDateTimeValue = Date.parse(dueDate) + new Date(Date.parse(dueDate)).getTimezoneOffset() + 1000 * dueTime;
    if (!Number.isNaN(dueDateTimeValue)) {
      if (date.valueOf() + releaseTime > dueDateTimeValue) {
        setReleaseDateValidated('warning');
        setReleaseDateHelperText('Release date and time cannot be after due date');
        return;
      }
    }
    setReleaseDateValidated('success');
    setReleaseDateHelperText('');
  }

  const onReleaseTimeChange = (time: string, hour?: number | undefined, minute?: number | undefined, seconds?: number | undefined, isValid?: boolean | undefined) => {
    if (isValid) {
      if (hour != undefined && minute != undefined) {
        const releaseTime = hour * 3600 + minute * 60;
        setReleaseTime(releaseTime);
        const dueDateTimeValue = Date.parse(dueDate) + new Date(Date.parse(dueDate)).getTimezoneOffset() + 1000 * dueTime;
        const releaseDateTimeValue = Date.parse(releaseDate) + new Date(Date.parse(releaseDate)).getTimezoneOffset() + 1000 * releaseTime;
        if (!Number.isNaN(dueDateTimeValue)) {
          if (dueDateTimeValue < releaseDateTimeValue) {
            setReleaseDateValidated('warning');
            setReleaseDateHelperText('Release date and time cannot be before due date and time');
            return;
          }
        }
        setReleaseDateValidated('success');
        setReleaseDateHelperText('');
      } else {
        setReleaseDateValidated('error');
        setReleaseDateHelperText('');
      }
    } else {
      setReleaseDateValidated('error');
      setReleaseDateHelperText('');
    }
  }

  const handleVisibility = (checked: boolean, event: React.FormEvent<HTMLInputElement> | undefined) => {
    if (!event) {
      if (visibility == '') {
        setVisibilityValidated('error');
      }
    } else {
      switch (event.currentTarget.id) {
        case 'visibility-radio-1':
          setVisibility("all");
          setVisibilityValidated('success');
          break;
        case 'visibility-radio-2':
          setVisibility("prof");
          setVisibilityValidated('success');
          break;
        default:
          setVisibility("");
          setVisibilityValidated('error');
          break;
      }
    }
  };

  const handleDelete = async (_event) => {
    setIsModalOpen(true);
  };

  const handleModalClose = () => {
    setIsModalOpen(false);
  };

  const handleConfirmAction = async () => {
    await client.deleteAssignment({
      classId: classId,
      assignmentId: assignmentId,
    }).then(() => {
      setIsModalOpen(false);
      history.back();
    })
  };

  const handleSubmit = async (_event) => {
    // Check for validity before proceeding
    if (nameValidated != 'success'
      || materialValidated != 'success'
      || dueDateValidated != 'success'
      || releaseDateValidated != 'success'
      || visibility == '') {
      console.log(nameValidated, materialValidated, dueDateValidated, releaseDateValidated, visibility)
      handleAssignmentName(assignmentName);
      onSelect(selected);
      onDueDateChange(dueDate, Number.isNaN(Date.parse(dueDate)) ? undefined : new Date(Date.parse(dueDate)))
      onReleaseDateChange(releaseDate, Number.isNaN(Date.parse(releaseDate)) ? undefined : new Date(Date.parse(releaseDate)))
      handleVisibility(true, undefined);
      setFormValidated('error');
      return;
    } else {
      setFormValidated('success');
    }

    // Use some regex to prevent injection, probably
    const selectedMat = materials.get(selected);

    const dueDateDate = new Date(Date.parse(dueDate));

    const dueDateTimestamp = Timestamp.fromDate(new Date(dueDateDate.valueOf() + 60000 * dueDateDate.getTimezoneOffset() + 1000 * dueTime));

    const releaseDateDate = new Date(Date.parse(releaseDate));

    const releaseDateTimestamp = Timestamp.fromDate(new Date(releaseDateDate.valueOf() + 60000 * releaseDateDate.getTimezoneOffset() + 1000 * releaseTime));

    // Update Assignment
    await client.updateAssignment({
      assignmentId: assignmentId,
      classId: classId,
      displayName: assignmentName,
      material: selectedMat,
      isVisibleToAll: visibility === "all",
      dueDate: dueDateTimestamp,
      releaseDate: releaseDateTimestamp,
      submissions: assignment?.submissions,
    }).then(response => response)

    // Clear everything
    setAssignmentName("");
    setSelected("");
    setDueDate("");
    setReleaseDate("");
    setVisibility("");
  }

  return <React.Fragment>
    <PageSection>
      <Breadcrumb>
        <BreadcrumbItem to="/edu/teaching">Manage Classes</BreadcrumbItem>
        <BreadcrumbItem to={`/class/${classId}`}>Class {classId}</BreadcrumbItem>
        <BreadcrumbItem isActive>Add Materials to {classId}</BreadcrumbItem>
      </Breadcrumb>
      <Title headingLevel="h1">
        Add materials to {classId}
      </Title>
      <Modal
        variant={ModalVariant.small}
        title="Confirm Delete?"
        isOpen={isModalOpen}
        onClose={handleModalClose}
        actions={[
          <Button key="confirm" variant="primary" onClick={handleConfirmAction}>
            Yes
          </Button>,
          <Button key="cancel" variant="link" onClick={handleModalClose}>
            No
          </Button>,
        ]}
      >
        {`Are you sure you want to delete "${assignmentName}"?`}
      </Modal>
      <Form>
        {formValidated === 'error' && (
          <FormAlert>
            <Alert variant="danger" title="Fill out all required fields before continuing." aria-live="polite" isInline />
          </FormAlert>
        )}
        {formValidated === 'warning' && (
          <FormAlert>
            <Alert variant="danger" title="Assignment already exists." aria-live="polite" isInline />
          </FormAlert>
        )}
        {formValidated === 'success' && (
          <FormAlert>
            <Alert variant="success" title="Successfully added assignment." aria-live="polite" isInline />
          </FormAlert>
        )}
        <FormGroup
          label="Assignment Name"
          isRequired
          fieldId="add-assignment-assignment-name"
          helperTextInvalid={"Assignment name cannot be empty"}
          helperTextInvalidIcon={<ExclamationCircleIcon />}
          validated={nameValidated}
        >
          <TextInput
            validated={nameValidated}
            isRequired
            type="text"
            id="add-assignment-assignment-name-input"
            name="add-assignment-assignment-name-input"
            value={assignmentName}
            onChange={handleAssignmentName}
          />
        </FormGroup>
        <FormGroup
          label="Materials Ready to Assign"
          isRequired
          fieldId="add-assignment-materials-ready-to-assign"
          helperTextInvalid={"Please select a material"}
          helperTextInvalidIcon={<ExclamationCircleIcon />}
          validated={materialValidated}
        >
          <FormSelect
            validated={materialValidated}
            onChange={onSelect}
            aria-label="FormSelect Input"
            value={selected}
          >{dropdownItems}</FormSelect>
        </FormGroup>
        <FormGroup
          label="Due Date"
          isRequired
          fieldId="add-assignment-due-date-picker"
          helperText={dueDateHelperText}
          helperTextInvalid={"Please select a valid due date and time"}
          helperTextInvalidIcon={<ExclamationCircleIcon />}
          validated={dueDateValidated}
        >
          <DatePicker
            value={dueDate}
            onChange={onDueDateChange}
          />
          <TimePicker onChange={onDueTimeChange} />
        </FormGroup>
        <FormGroup
          label="Release Date"
          isRequired
          fieldId="add-assignment-release-date-picker"
          helperText={releaseDateHelperText}
          helperTextInvalid={"Please select a valid release date and time"}
          helperTextInvalidIcon={<ExclamationCircleIcon />}
          validated={releaseDateValidated}
        >
          <DatePicker
            value={releaseDate}
            onChange={onReleaseDateChange}
          />
          <TimePicker onChange={onReleaseTimeChange} />

        </FormGroup>
        <FormGroup
          label="Visibility"
          isRequired
          role="radiogroup"
          isInline
          fieldId="visibility-radio-group"
          helperTextInvalid={"Select visibility"}
          helperTextInvalidIcon={<ExclamationCircleIcon />}
          validated={visibilityValidated}
        >
          <Radio name="basic-inline-radio" onChange={handleVisibility} isChecked={visibility === "all"} label="All" id="visibility-radio-1" />
          <Radio name="basic-inline-radio" onChange={handleVisibility} isChecked={visibility === "prof"} label="Professors/TAs Only" id="visibility-radio-2" />
        </FormGroup>
        <ActionGroup>
          <Button variant="primary" onClick={handleSubmit}>Add</Button>
          <Button variant="link" onClick={() => history.back()}>Cancel</Button>
          <Button variant="danger" onClick={handleDelete}>Delete</Button>
        </ActionGroup>
      </Form>
    </PageSection>
  </React.Fragment>
};

export { AssignmentPage };


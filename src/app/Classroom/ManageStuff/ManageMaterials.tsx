import * as React from "react";
import { GeneralSettingsContext } from "@app/Settings/General/GeneralSettings";
import { PageSection, Title } from "@patternfly/react-core";
import { Material } from "@buf/sphere_edu.bufbuild_es/edu/v1/edu_types_pb";
import { GetMaterialsResponse } from "@buf/sphere_edu.bufbuild_es/edu/v1/edu_pb";
import { Link, useParams } from "react-router-dom";
import { TableComposable, Tbody, Td, Th, Thead, Tr } from "@patternfly/react-table";

type ManageMaterialsProp = {
  classId: string;
}

const ManageMaterials: React.FunctionComponent = () => {
  const { classId } = useParams<ManageMaterialsProp>();
  const conf = React.useContext(GeneralSettingsContext);

  const [materials, setMaterials] = React.useState<Material[]>([]);

  React.useMemo(async () => {
    const data = await fetch(`${conf.eduApi}/edu.v1.EduService/GetMaterials`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        classId: classId,
      }),
      credentials: 'include',
      cache: 'no-cache',
    }).then(response => response.json())

    if (Object.hasOwnProperty.call(data, 'materials')) {
      setMaterials(GetMaterialsResponse.fromJson(data).materials);
    }
  }, [conf, classId]);

  return <React.Fragment>
    <PageSection>
      <Title headingLevel="h1">
        Manage Materials
      </Title>
      Click on a material below to edit it
      <TableComposable variant="compact" borders={false}>
        <Thead>
          <Tr>
            <Th>
              Material Name
            </Th>
          </Tr>
        </Thead>
        {materials.map((mat, i) => {
          return (<Tbody key={`manage-materials-table-body-${i}`}>
            <Tr>
              <Td dataLabel={"Material Name"}>
                <Link to={`/class/${classId}/material/${mat.materialId}`}>
                  {mat.displayName}
                </Link>
              </Td>
            </Tr>
          </Tbody>)
        })}
      </TableComposable>
    </PageSection>
  </React.Fragment>
};

export { ManageMaterials };
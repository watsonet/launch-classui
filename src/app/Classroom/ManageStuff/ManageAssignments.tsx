import * as React from "react";
import { GeneralSettingsContext } from "@app/Settings/General/GeneralSettings";
import { PageSection, Title } from "@patternfly/react-core";
import { Assignment } from "@buf/sphere_edu.bufbuild_es/edu/v1/edu_types_pb";
import { GetAssignmentsResponse } from "@buf/sphere_edu.bufbuild_es/edu/v1/edu_pb";
import { Link, useParams } from "react-router-dom";
import { TableComposable, Tbody, Td, Th, Thead, Tr } from "@patternfly/react-table";
import { EduService } from "@buf/sphere_edu.connectrpc_es/edu/v1/edu_connect";
import { createPromiseClient } from "@connectrpc/connect"
import { createConnectTransport } from '@connectrpc/connect-web'

type ManageAssignmentsProp = {
  classId: string;
}

const ManageAssignments: React.FunctionComponent = () => {
  const { classId } = useParams<ManageAssignmentsProp>();
  const conf = React.useContext(GeneralSettingsContext);
  const transport = createConnectTransport({
    baseUrl: `${conf.eduApi}`,
  })
  const client = createPromiseClient(EduService, transport);

  const [assignments, setAssignments] = React.useState<Assignment[]>([]);

  React.useMemo(async () => {
    await client.getAssignments({
      classId: classId,
    }).then(response => {
      setAssignments(response.assignments);
    })
  }, [conf, classId]);

  return <React.Fragment>
    <PageSection>
      <Title headingLevel="h1">
        Manage Assignments
      </Title>
      Click on an assignment below to edit it
      <TableComposable variant="compact" borders={false}>
        <Thead>
          <Tr>
            <Th>
              Assignment Name
            </Th>
          </Tr>
        </Thead>
        {assignments.map((a, i) => {
          return (<Tbody key={`manage-materials-table-body-${i}`}>
            <Tr>
              <Td dataLabel={"Assignment Name"}>
                <Link to={`/class/${classId}/assignment/${a.assignmentId}`}>
                  {a.displayName}
                </Link>
              </Td>
            </Tr>
          </Tbody>)
        })}
      </TableComposable>
    </PageSection>
  </React.Fragment>
};

export { ManageAssignments };
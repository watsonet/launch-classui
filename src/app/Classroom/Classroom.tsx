import { GeneralSettingsContext } from '@app/Settings/General/GeneralSettings';
import { Alert, Breadcrumb, BreadcrumbItem, Bullseye, PageSection, Spinner, Title } from '@patternfly/react-core';
import * as React from 'react';
import { Link, useParams } from 'react-router-dom';
import { useFetch } from 'use-http';
import { Class, UserType } from '@buf/sphere_edu.bufbuild_es/edu/v1/edu_types_pb'
import { GetClassResponse } from '@buf/sphere_edu.bufbuild_es/edu/v1/edu_pb'
import { TableComposable, Tbody, Td, Th, Thead, Tr } from '@patternfly/react-table';
import { AuthContext } from '@app/lib/AuthProvider';

type ClassroomProp = {
  oid: string;
}

const Classroom: React.FunctionComponent<ClassroomProp> = () => {
  const { identity } = React.useContext(AuthContext);
  const username = identity?.traits.username;

  const { oid } = useParams<ClassroomProp>();
  const conf = React.useContext(GeneralSettingsContext);
  const [classroom, setClassroom] = React.useState<Class>();
  const [response, setResponse] = React.useState<Response>();

  React.useMemo(async () => {
    const data = await fetch(`${conf.eduApi}/edu.v1.EduService/GetClass`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        classId: oid,
      }),
      credentials: 'include',
      cache: 'no-cache',
    }).then(response => {
      setResponse(response);
      return response.json();
    })
    // console.log(data);

    if (Object.hasOwnProperty.call(data, 'class')) {
      setClassroom(GetClassResponse.fromJson(data).class);
    }
  }, [oid]);

  if (classroom == undefined) {
    if (response && response.ok) {
      return <React.Fragment>
        <PageSection>
          <Alert variant="danger" title="Not Found Error">
            <pre>This class does not exist</pre>
          </Alert>
        </PageSection>
      </React.Fragment>
    } else {
      return <React.Fragment>
        <Bullseye>
          <Spinner size='xl' />
        </Bullseye>
      </React.Fragment>
    }
  } else if (classroom.users[username] == undefined || classroom.users[username] == UserType.STUDENT || classroom.users[username] == UserType.UNSPECIFIED) {
    return <React.Fragment>
      <PageSection>
        <Alert variant="danger" title="Permission Error">
          <pre>You do not have permission to access this content</pre>
        </Alert>
      </PageSection>
    </React.Fragment>
  }

  const cols = [
    "Users",
    "Materials",
    "Assignments",
  ]

  return (<React.Fragment>
    <PageSection>
      <Breadcrumb>
        <BreadcrumbItem to="/edu/teaching">Manage Classes</BreadcrumbItem>
        <BreadcrumbItem isActive>
          Class {oid}
        </BreadcrumbItem>
      </Breadcrumb>
      <Title headingLevel='h1'>
        Manage class <Link to={`/organization/${oid}`}>{oid}</Link>
      </Title>
      <TableComposable variant={'compact'} borders={false}>
        <Thead>
          <Tr>
            {cols.map((c) => {
              return <Th key={`class-table-head-${c.toLowerCase()}`} hasRightBorder>{c}</Th>
            })}
          </Tr>
        </Thead>
        <Tbody>
          <Tr>
            {cols.map((c) => {
              return (
                <Td key={`class-table-body-add-${c.toLowerCase()}`} dataLabel={c}>
                  <Link to={`/class/${oid}/add${c.toLowerCase()}`}>
                    {`Add ${c}`}
                  </Link>
                </Td>
              )
            })}
          </Tr>
          <Tr>
            {cols.map((c, i) => {
              return (
                <Td key={`class-table-body-manage-${c.toLowerCase()}`} dataLabel={c}>
                  <Link to={`/class/${oid}/${c.toLowerCase()}`}>
                    {`Manage ${c}`}
                  </Link>
                </Td>
              )
            })}
          </Tr>
        </Tbody>
      </TableComposable>
    </PageSection>
  </React.Fragment>);
};

export { Classroom };
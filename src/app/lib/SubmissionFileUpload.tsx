import * as React from "react";
import { FileUpload } from "@patternfly/react-core";
import { UploadFileRequest, UploadFileResponse } from "@buf/sphere_edu.bufbuild_es/edu/v1/edu_pb"
import { EduService } from "@buf/sphere_edu.connectrpc_es/edu/v1/edu_connect";
import { createPromiseClient } from "@connectrpc/connect"
import { createConnectTransport } from '@connectrpc/connect-web'
import { FileData } from "@buf/sphere_edu.bufbuild_es/edu/v1/edu_types_pb"
import { GeneralSettingsContext } from "@app/Settings/General/GeneralSettings";
import fetch from 'node-fetch';

type SubmissionFileUploadProps = {
  classId: string,
  userId: string,
  assignmentId: string,
}

const chunkSize = 5 * 1024 * 1024; // 5MB (adjust based on your requirements)

const SubmissionFileUpload: React.FunctionComponent<SubmissionFileUploadProps> = ({ classId, userId, assignmentId }) => {
  const conf = React.useContext(GeneralSettingsContext);
  const transport = createConnectTransport({
    baseUrl: `${conf.eduApi}`,
  })
  const client = createPromiseClient(EduService, transport);
  const [value, setValue] = React.useState<File>();
  const [filename, setFilename] = React.useState('');
  const [status, setStatus] = React.useState("");
  const [progress, setProgress] = React.useState(0);

  // Generator function that gives the next chunk of a file (use if client streaming ever gets added to the fetch api)
  // async function* generateChunk(file: File, chunkNumber: number) {
  //   const start = chunkNumber * chunkSize;
  //   const fileReader = file.stream().getReader();
  //   const { done, value } = await fileReader.read();
  //   if (value) {
  //     value.slice(start, start + chunkSize);
  //   }
  //   yield new UploadFileRequest({
  //   })
  // }

  // Regular function that gives the next chunk of a file (No client streaming support yet >:[ )
  async function generateChunk(file: File, chunkNumber: number, totalChunks: number) {
    const start = chunkNumber * chunkSize;
    const fileReader = file.stream().getReader();
    const { done, value } = await fileReader.read();
    let chunk: Uint8Array = new Uint8Array();
    if (value) {
      chunk = value.slice(start, start + chunkSize);
    } else {
      console.error("SOmething went wrong in reading the file");
      return new UploadFileRequest();
    }
    return new UploadFileRequest({
      fileId: userId,
      bucketId: `${classId}-${assignmentId}`,
      totalChunks: totalChunks,
      chunkNumber: chunkNumber,
      data: chunk,
    })
  }

  const handleUploadFile = async (_event: any, file: File) => {
    setValue(file);
    setFilename(file.name);

    // Create Submission
    client.uploadSubmission({
      userId: userId,
      classId: classId,
      assignmentId: assignmentId,
      filename: file.name,
    }).catch(error => error);

    // Create FileData

    const totalChunks = Math.ceil(file.size / chunkSize);
    const chunkProgress = 100 / totalChunks;

    for (let chunkNumber = 0; chunkNumber < totalChunks; chunkNumber++) {
      await client.uploadFile(await generateChunk(file, chunkNumber, totalChunks))
    }

    // const body = (async function* () {
    //   for await (const chunk of generateChunk(file, chunkNumber)) {
    //     if (chunkNumber >= totalChunks) {
    //       break;
    //     }
    //     console.log(chunk)
    //     chunkNumber++;
    //     yield chunk;
    //   }
    // })();

    // const fdResp = await fetch(`${conf.eduApi}/edu.v1.EduService/UploadFile`, {
    //   method: 'POST',
    //   headers: {
    //     'Content-Type': 'application/connect+json',
    //   },
    //   body: body,
    // })

    // if (fdResp.body) {
    //   const reader = fdResp.body.getReader();
    //   const decoder = new TextDecoder('utf-8');
    //   while (true) {
    //     const { done, value } = await reader.read();
    //     if (done) break;
    //     const response = UploadFileResponse.fromBinary(value);
    //     console.log('Server response:', response.toJson());
    //   }
    // } else {
    //   console.error('No response body');
    // }
  }

  const handleClear = async () => {
    setValue(undefined);
    setFilename("");
  }

  return <FileUpload
    id={`${assignmentId}-file-upload`}
    value={value}
    filename={filename}
    filenamePlaceholder="Drag and drop a file or upload one"
    onFileInputChange={handleUploadFile}
    onClearClick={handleClear}
    browseButtonText="Upload"
  />
}

export { SubmissionFileUpload };